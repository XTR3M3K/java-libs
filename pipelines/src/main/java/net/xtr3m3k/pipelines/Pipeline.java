package net.xtr3m3k.pipelines;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Pipeline {
    private ExecutorService service;
    private String name;
    private HashMap<String, PipelineBranch> branches;

    public Pipeline(String name) {
        this.name = name;
        this.branches = new HashMap<>();
        this.branches.put("master", new PipelineBranch("master", this));
        this.service = Executors.newSingleThreadExecutor();
    }

    public Pipeline(String name, ExecutorService executorService) {
        this.name = name;
        this.branches = new HashMap<>();
        this.branches.put("master", new PipelineBranch("master", this));
        this.service = executorService;
    }

    public String getName() {
        return name;
    }

    public HashMap<String, PipelineBranch> getBranches() {
        return branches;
    }

    public void start(String branch, Object feed) {
        if (!branches.containsKey(branch))
            throw new RuntimeException("Branch \"" + branch + "\" doesn't exists in pipeline " + name);
        branches.get(branch).emit(feed);
    }

    public void startAsync(String branch, Object feed) {
        if (!branches.containsKey(branch))
            throw new RuntimeException("Branch \"" + branch + "\" doesn't exists in pipeline " + name);
        service.execute(() -> branches.get(branch).emit(feed));
    }
}
