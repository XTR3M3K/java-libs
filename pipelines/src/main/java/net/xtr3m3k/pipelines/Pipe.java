package net.xtr3m3k.pipelines;

public class Pipe<T> {
    protected PipelineBranch parent;
    private String name;
    private String friendlyName;

    public Pipe(String name) {
        this.name = name;
    }

    public void emit(T feed) {
    }

    public void breakPipeline() {
        parent.lock();
    }

    public String getName() {
        return name;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setParent(PipelineBranch parent) {
        if (this.parent != null)
            throw new RuntimeException("This pipe is belonging to branch \"" + parent.getName() + "\"");
        this.parent = parent;
    }
}
