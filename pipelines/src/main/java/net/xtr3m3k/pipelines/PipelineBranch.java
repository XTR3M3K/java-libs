package net.xtr3m3k.pipelines;

import java.util.ArrayList;
import java.util.List;

public class PipelineBranch {
    private final List<Pipe> pipes;
    private String name;
    private Pipeline parent;
    private boolean lock;

    public PipelineBranch(String name, Pipeline parent) {
        this.name = name;
        this.pipes = new ArrayList<>();
        this.parent = parent;
        this.lock = false;
        parent.getBranches().put(name, this);
    }

    public void emit(Object feed) {
        synchronized (pipes) {
            for (Pipe pipe : pipes) {
                if (lock) break;
                pipe.emit(feed);
            }
        }
    }

    public PipelineBranch addLast(Pipe pipe) {
        pipes.add(pipe);
        pipe.setParent(this);
        return this;
    }

    public PipelineBranch addFirst(Pipe pipe) {
        pipes.add(0, pipe);
        pipe.setParent(this);
        return this;
    }

    public int indexOf(Pipe pipe) {
        return pipes.indexOf(pipe);
    }

    public int indexOf(String name) {
        for (Pipe pipe : pipes) {
            if (pipe.getName().equals(name)) {
                return indexOf(pipe);
            }
        }
        return -1;
    }

    public PipelineBranch addPast(Pipe target, Pipe pipe) {
        pipes.add(indexOf(target) + 1, pipe);
        pipe.setParent(this);
        return this;
    }

    public PipelineBranch addBefore(Pipe target, Pipe pipe) {
        pipes.add(indexOf(target), pipe);
        pipe.setParent(this);
        return this;
    }

    public Pipe remove(Pipe target) {
        return pipes.remove(indexOf(target));
    }

    public Pipe remove(int i) {
        return pipes.remove(i);
    }

    public PipelineBranch replace(Pipe target, Pipe pipe) {
        pipes.add(indexOf(target), pipe);
        pipe.setParent(this);
        return this;
    }

    public String getName() {
        return name;
    }

    public List<Pipe> getPipes() {
        return pipes;
    }

    public Pipeline getParent() {
        return parent;
    }

    public void lock() {
        this.lock = true;
    }
}
