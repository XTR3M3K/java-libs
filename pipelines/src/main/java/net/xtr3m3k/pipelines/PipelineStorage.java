package net.xtr3m3k.pipelines;

import java.util.concurrent.ConcurrentHashMap;

public class PipelineStorage {
    private ConcurrentHashMap<String, Object> storage;

    public PipelineStorage() {
        this.storage = new ConcurrentHashMap<>();
    }

    public ConcurrentHashMap<String, Object> getStorage() {
        return storage;
    }
}
