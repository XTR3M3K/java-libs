package net.xtr3m3k.servlets;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class HttpServlet {
    private HashMap<String, HttpServlet> mappings = new HashMap<>();

    public void register(HttpServlet servlet) {
        Class<? extends HttpServlet> clazz = servlet.getClass();
        if (!clazz.isAnnotationPresent(Servlet.class))
            throw new IllegalArgumentException("Servlet must have Servlet.class annotation!");
        mappings.put(clazz.getAnnotation(Servlet.class).path(), servlet);
    }

    public void execute(String[] path, HttpExchange httpExchange) throws IOException {
        if (path.length == 1) {
            System.out.println("Path is ended here. Firing properly request method handler!");
            switch (httpExchange.getRequestMethod()) {
                case "GET":
                    doGet(httpExchange);
                    break;
                case "POST":
                    doPost(httpExchange);
                    break;
                case "PUT":
                    doPut(httpExchange);
                    break;
                case "DELETE":
                    doDelete(httpExchange);
                    break;
                case "PATCH":
                    doPatch(httpExchange);
                    break;
                case "OPTIONS":
                    httpExchange.sendResponseHeaders(200, 0);
                    httpExchange.close();
                    break;
            }
            System.out.println("==========");
            return;
        }
        System.out.println("Searching /" + path[1] + " ...");
        HttpServlet servlet = mappings.get("/" + path[1]);
        if (servlet == null) {
            System.out.println("/" + path[1] + " not found!");
            httpExchange.sendResponseHeaders(200, 0);
            httpExchange.close();
            return;
        }
        System.out.println("Found! Executing servlet at /" + path[1]);
        System.out.println("==========");
        servlet.execute(Arrays.copyOfRange(path, 1, path.length), httpExchange);
    }

    protected void doPatch(HttpExchange httpExchange) {}

    protected void doDelete(HttpExchange httpExchange) {}

    protected void doPut(HttpExchange httpExchange) {}

    protected void doPost(HttpExchange httpExchange) {}

    protected void doGet(HttpExchange httpExchange) throws IOException {}
}
