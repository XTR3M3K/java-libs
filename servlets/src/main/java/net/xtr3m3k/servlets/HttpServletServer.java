package net.xtr3m3k.servlets;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.Executors;

public class HttpServletServer implements HttpHandler {
    private static boolean cors = true;
    private HashMap<String, HttpServlet> mappings = new HashMap<>();
    private HttpServer httpServer;

    public static void main(String[] args) {
        String url = "/path1/path2/path3/path4";
        String[] split = url.split("/");
        System.out.println(Arrays.toString(Arrays.copyOfRange(split, 1, split.length)));
    }

    public static boolean isCors() {
        return cors;
    }

    public static void setCors(boolean cors) {
        HttpServletServer.cors = cors;
    }

    public void register(HttpServlet servlet) {
        Class<? extends HttpServlet> clazz = servlet.getClass();
        if (!clazz.isAnnotationPresent(Servlet.class))
            throw new IllegalArgumentException("Servlet must have Servlet.class annotation!");
        mappings.put(clazz.getAnnotation(Servlet.class).path(), servlet);
    }

    public void start(String host, int port) {
        try {
            httpServer = HttpServer.create(new InetSocketAddress(host, port), 0);
            httpServer.setExecutor(Executors.newSingleThreadExecutor());
            httpServer.createContext("/", this);
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override public void handle(HttpExchange httpExchange) throws IOException {
        String[] split = httpExchange.getRequestURI().toString().split("/");
        System.out.println("==========");
        System.out.println(httpExchange.getRequestMethod() + " " + httpExchange.getRequestURI());
        if (split.length == 1) {
            httpExchange.sendResponseHeaders(200, 0);
            httpExchange.close();
            return;
        }
        System.out.println("Searching mapping of /" + split[1]);
        HttpServlet servlet = mappings.get("/" + split[1]);
        if (servlet == null) {
            System.out.println("Mapping /" + split[1] + " not found!");
            httpExchange.sendResponseHeaders(200, 0);
            httpExchange.close();
            return;
        }
        System.out.println("==========");
        System.out.println("Executing servlet mapped at /" + split[1]);
        System.out.println("==========");
        servlet.execute(Arrays.copyOfRange(split, 1, split.length), httpExchange);
    }
}
