package net.xtr3m3k.database;

public interface Callback<T> {
    void done(Throwable err, T data);
}
