package net.xtr3m3k.database.collections;

import net.xtr3m3k.database.Callback;
import net.xtr3m3k.database.Model;
import org.bson.Document;

interface Collection<T extends Model> {
    void get(Document document, Callback<T> callback);

    void getAll(Callback<T[]> callback);

    void insert(T data, Callback<T> callback);

    void update(T data, Callback<T> callback);

    void delete(T data);
}
