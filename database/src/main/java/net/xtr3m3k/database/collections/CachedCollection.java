package net.xtr3m3k.database.collections;

import net.xtr3m3k.database.Callback;
import net.xtr3m3k.database.Model;
import org.bson.Document;

public class CachedCollection<T extends Model> implements Collection<T> {
    @Override public void get(Document document, Callback<T> callback) {

    }

    @Override public void getAll(Callback<T[]> callback) {

    }

    @Override public void insert(T data, Callback<T> callback) {

    }

    @Override public void update(T data, Callback<T> callback) {

    }

    @Override public void delete(T data) {

    }
}
