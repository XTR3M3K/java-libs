package net.xtr3m3k.database;

import net.xtr3m3k.database.collections.RemoteCollection;

public class Model {
    private RemoteCollection<Model> collection;

    public void insert(Callback<Model> callback) {
        collection.insert(this, callback);
    }

    public void update(Callback<Model> callback) {
        collection.update(this, callback);
    }
}
